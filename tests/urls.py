# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from django.conf.urls import url, include


urlpatterns = [
    url(r'^api', include('exo_backup.api.urls', namespace='api-exo-backup')),
    url(r'^', include('exo_backup.urls', namespace='exo_backup')),
]
