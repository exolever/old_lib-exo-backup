#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_lib-exo-backup
------------

Tests for `lib-exo-backup` models module.
"""

from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse

from exo_backup import models

from rest_framework.test import APITestCase
from rest_framework import status
from rest_framework.authtoken.models import Token


User = get_user_model()


class TestAPIExoBackup(APITestCase):

    def setUp(self):
        pass

    def do_login(self):
        user = User.objects.create(
            username='example',
            email='example@example.com',
            is_active=True,
            is_staff=True)
        user.set_password('123456')
        user.save()
        self.client.login(username=user.username, password='123456')
        return user

    def test_create(self):
        # PREPARE DATA
        user = self.do_login()
        url = reverse('api-exo-backup:generate')

        # DO ACTION
        response = self.client.post(url, data={})

        # ASSERTS
        self.assertTrue(status.is_success(response.status_code))
        backup = models.Backup.objects.first()
        self.assertEqual(backup.created_by, user)

    def test_update(self):
        # PREPARE DATA
        user = self.do_login()
        backup = models.Backup.objects.create(created_by=user)
        url = reverse('api-exo-backup:update', kwargs={'pk': backup.pk})
        token, _ = Token.objects.get_or_create(user=user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        # DO ACTION
        response = self.client.put(url, data={'s3_file': 'filename.tar.gz'})

        # ASSERTS
        self.assertTrue(status.is_success(response.status_code))
        backup.refresh_from_db()
        self.assertEqual(backup.s3_file, 'filename.tar.gz')
        self.assertTrue(backup.is_ready)
        self.assertIsNotNone(backup.url_path)
        self.assertIsNotNone(backup.generated_at)

    def tearDown(self):
        pass
