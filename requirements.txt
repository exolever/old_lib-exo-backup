django-model-utils>=2.0
# Additional requirements go here
django-bootstrap-breadcrumbs==0.8.2
django-bootstrap-pagination==1.6.2
djangorestframework==3.6.3
