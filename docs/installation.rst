============
Installation
============

At the command line::

    $ easy_install lib-exo-backup

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv lib-exo-backup
    $ pip install lib-exo-backup
