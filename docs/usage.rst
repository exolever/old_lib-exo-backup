=====
Usage
=====

To use lib-exo-backup in a project, add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'exo_backup.apps.ExoBackupConfig',
        ...
    )

Add lib-exo-backup's URL patterns:

.. code-block:: python

    from exo_backup import urls as exo_backup_urls


    urlpatterns = [
        ...
        url(r'^', include(exo_backup_urls)),
        ...
    ]
