=============================
lib-exo-backup
=============================

.. image:: https://badge.fury.io/py/lib-exo-backup.svg
    :target: https://badge.fury.io/py/lib-exo-backup

.. image:: https://travis-ci.org/tomasgarzon/lib-exo-backup.svg?branch=master
    :target: https://travis-ci.org/tomasgarzon/lib-exo-backup

.. image:: https://codecov.io/gh/tomasgarzon/lib-exo-backup/branch/master/graph/badge.svg
    :target: https://codecov.io/gh/tomasgarzon/lib-exo-backup

Manage backups for our django database

Documentation
-------------

The full documentation is at https://lib-exo-backup.readthedocs.io.

Quickstart
----------

Install lib-exo-backup::

    pip install lib-exo-backup

Add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'exo_backup.apps.ExoBackupConfig',
        ...
    )

Add lib-exo-backup's URL patterns:

.. code-block:: python

    from exo_backup import urls as exo_backup_urls


    urlpatterns = [
        ...
        url(r'^', include(exo_backup_urls)),
        ...
    ]

Features
--------

* TODO

Running Tests
-------------

Does the code actually work?

::

    source <YOURVIRTUALENV>/bin/activate
    (myenv) $ pip install tox
    (myenv) $ tox

Credits
-------

Tools used in rendering this package:

*  Cookiecutter_
*  `cookiecutter-djangopackage`_

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`cookiecutter-djangopackage`: https://github.com/pydanny/cookiecutter-djangopackage
