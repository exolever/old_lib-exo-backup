# -*- coding: utf-8 -*-
from django.conf.urls import url

from . import views


app_name = 'exo_backup'
urlpatterns = [
    url(
        regex="^backup/$",
        view=views.BackupListView.as_view(),
        name='list',
    ),
]
