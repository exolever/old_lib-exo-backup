import logging

from rest_framework import serializers

from django.utils import timezone

from ..models import Backup
from ..signals_define import signal_backup_generated

logger = logging.getLogger('backup')


class BackupUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Backup
        fields = ['s3_file', ]

    def update(self, instance, validated_data):
        instance.s3_file = validated_data.get('s3_file')
        instance.generated_at = timezone.now()
        instance.status = instance.CH_DONE
        instance.save()
        logger.info('Received backup: {}'.format(instance.s3_file))
        signal_backup_generated.send_robust(
            sender=Backup,
            instance=instance)
        return instance
