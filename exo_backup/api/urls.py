# -*- coding: utf-8 -*-
from django.conf.urls import url

from . import views


urlpatterns = [
    url(
        r'^backup/generate/$', views.CreateBackupView.as_view(),
        name='generate',
    ),
    url(
        r'^backup/update/(?P<pk>\d+)/$', views.UpdateBackupView.as_view(),
        name='update',
    ),
]
