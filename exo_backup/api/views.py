from rest_framework.views import APIView
from rest_framework.generics import UpdateAPIView
from rest_framework.response import Response
from rest_framework import permissions
from rest_framework.authentication import TokenAuthentication, SessionAuthentication

from django.contrib import messages

from ..models import Backup
from ..signals_define import signal_backup_requested
from .serializers import BackupUpdateSerializer


class CreateBackupView(APIView):
    authentication_classes = (SessionAuthentication, )
    permission_classes = (permissions.IsAdminUser,)
    model = Backup

    def post(self, request, format=None):
        backup = Backup.objects.create(created_by=request.user)
        signal_backup_requested.send_robust(
            sender=Backup,
            instance=backup)
        messages.success(request, 'The backup is being generated, you\'ll receive an email')
        return Response('ok')


class UpdateBackupView(UpdateAPIView):
    authentication_classes = (TokenAuthentication, )
    permission_classes = (permissions.IsAuthenticated,)
    model = Backup
    serializer_class = BackupUpdateSerializer
    queryset = Backup.objects.all()
