from django.dispatch import Signal


signal_backup_requested = Signal(providing_args=['instance'])
signal_backup_generated = Signal(providing_args=['instance'])
