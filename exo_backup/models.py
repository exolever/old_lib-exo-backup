# -*- coding: utf-8 -*-

from django.db import models
from django.conf import settings

from model_utils.models import TimeStampedModel


class Backup(TimeStampedModel):
    CH_PENDING = 'P'
    CH_DONE = 'D'

    CH_STATUS = (
        (CH_PENDING, 'Pending'),
        (CH_DONE, 'Done'),
    )

    PATH = 'backups'

    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='%(app_label)s_%(class)s_related',
        null=True, blank=True,
    )
    status = models.CharField(
        max_length=1, choices=CH_STATUS,
        default=CH_PENDING)
    generated_at = models.DateTimeField(blank=True, null=True)
    s3_file = models.CharField(
        max_length=200, blank=True, null=True)

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return '{} - {}'.format(self.get_status_display(), self.created)

    @property
    def is_ready(self):
        return self.status == self.CH_DONE

    @property
    def url_path(self):
        return "/{}/{}".format(self.PATH, self.s3_file)
