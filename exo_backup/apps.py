# -*- coding: utf-8
from django.apps import AppConfig


class ExoBackupConfig(AppConfig):
    name = 'exo_backup'
