# -*- coding: utf-8 -*-
from django.views.generic import (
    ListView
)

from .models import (
    Backup,
)


class BackupListView(ListView):

    template_name = 'exo_backup/list.html'
    model = Backup
    paginate_by = 30
